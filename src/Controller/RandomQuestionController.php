<?php

namespace App\Controller;

use App\Entity\Questions;
use App\Repository\QuestionsRepository;

class RandomQuestionController
{

    /**
     * @var QuestionsRepository
     */
    protected QuestionsRepository $questionsRepository;

    public function __construct(QuestionsRepository $questionsRepository)
    {
        $this->questionsRepository = $questionsRepository;
    }

    public function __invoke(): Questions
    {
        $random = rand(1, 4);

        return $this->questionsRepository->find($random);
      
    }


}