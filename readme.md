# HappyQuiz

Little quiz game in Symfony & Twig to train.

## Development environement

### Before begin

* PHP 8.1.13
* Composer
* Symfony CLI
* Docker
* Docker-compose

You can check prerequies (except Docker and Docker-compose) with follow command (from Symfony CLI) :

```bash
symfony check:requirements
```

### Launch development environnement

```bash
docker-compose up -d
symfony serve -d
```
